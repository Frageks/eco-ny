import express, { Express, Request, Response, ErrorRequestHandler, NextFunction } from 'express';
import dotenv from 'dotenv';
import axios from 'axios';
const jstat = require('jstat');

const path = require('path');
const session = require('express-session');
const mysql = require('mysql');
const winston = require('winston'),
    expressWinston = require('express-winston');
const cors = require('cors');

dotenv.config();

const app: Express = module.exports = express();
const port = process.env.PORT;
app.use(express.json());

app.use(expressWinston.logger({
  transports: [
    new(winston.transports.File)({filename: '/var/log/logF.log'})
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.json()
  )
}));
app.use(express.static('public'));

type emptyCB = () => void;

interface User {
  name: string;
  salt: string;
  hash: string;
};

interface ExpressSession {
  destroy: (cb: emptyCB) => void;
  error: string | undefined,
  regenerate: (cb: emptyCB) => void;
  success: string | undefined;
  user: User | null;
}

interface Measurement {
  id: number;
  dust_1_0: number;
  dust_2_5: number;
  dust_10: number;
  CO2: number;
  temperature: number;
  humidity: number;
  wind_speed: number;
  direction: string;
  deviceId: string;
  datetime: number;
}

interface IGetUserAuthInfoRequest extends Request {
  session: ExpressSession;
}

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
});

connection.connect();

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '..', 'views'));

// middleware

app.use(express.urlencoded({ extended: false }))

function calculateAverageValue(data: number[]): number {
  const mean = (data.reduce((sum, current) => sum + current)) / data.length;
  return Number(mean.toFixed(5));
}

function calculateMedianValue(data: number[]) {
  data = [...data].sort((a, b) => a - b);
  const half = Math.floor(data.length / 2);

  return Number(data.length % 2
    ? data[half].toFixed(5)
    : ((data[half - 1] + data[half]) / 2).toFixed(5)
  );
}

function calculateVariance(data: number[]) {
  const average = calculateAverageValue(data);
  const squareDiffs = data.map((value: number): number => {
    const diff = value - average;
    return diff * diff;
  })

  const variance = calculateAverageValue(squareDiffs);
  return Number(variance.toFixed(5));
}

function calculateSD(data: number[]) {
  const variance = calculateVariance(data);
  return Number(Math.sqrt(variance).toFixed(5));
}

function ksTest(data: number[]) {
  const sortedData = data.slice().sort((a, b) => a - b);

  const ecdf = (x: number) => {
    const count = sortedData.filter((value) => value <= x).length;
    return count / data.length;
  };

  let ksStatistic = 0;
  for (let i = 0; i < data.length; i++) {
    const empiricalProbability = ecdf(sortedData[i]);
    const theoreticalProbability = (i + 1) / data.length;
    const difference = Math.abs(empiricalProbability - theoreticalProbability);
    if (difference > ksStatistic) {
      ksStatistic = difference;
    }
  }
  return Number(ksStatistic.toFixed(5));
}

function calculateCritical(data: number[]) {
  const criticalValue = 1.36 / Math.sqrt(data.length);
  return Number(criticalValue.toFixed(5));
}

function removeOutliers(row: number[], data: Measurement[], propName: keyof Measurement): Measurement[] {
  const mean = calculateAverageValue(row);
  const standardDeviation = Math.sqrt(row.reduce((sum, value) => sum + Math.pow(value - mean, 2), 0) / row.length);
  const zScoreThreshold = 3;
  return data.filter(value => Math.abs((value[propName] as number - mean) / standardDeviation) <= zScoreThreshold);
}

function clearData(rows: Measurement[]) {
  let filteredRows: Measurement[] = [];
  filteredRows = removeOutliers(rows.map(row => row.dust_1_0), rows, 'dust_1_0' as keyof Measurement);
  filteredRows = removeOutliers(rows.map(row => row.dust_2_5), rows, 'dust_2_5' as keyof Measurement);
  filteredRows = removeOutliers(rows.map(row => row.dust_10), rows, 'dust_10' as keyof Measurement);
  filteredRows = removeOutliers(rows.map(row => row.CO2), rows, 'CO2' as keyof Measurement);
  filteredRows = removeOutliers(rows.map(row => row.temperature), rows, 'temperature' as keyof Measurement);
  filteredRows = removeOutliers(rows.map(row => row.humidity), rows, 'humidity' as keyof Measurement);
  filteredRows = removeOutliers(rows.map(row => row.wind_speed), rows, 'wind_speed' as keyof Measurement);
  return filteredRows;
}

const findCorrelation = (x: number[], y: number[]) => {
  const xMean = calculateAverageValue(x);
  const yMean = calculateAverageValue(y);

  const numerator = x.reduce((sum, xi, index) => sum + (xi - xMean) * (y[index] - yMean), 0);
  const denominatorX = x.reduce((sum, xi) => sum + Math.pow(xi - xMean, 2), 0);
  const denominatorY = y.reduce((sum, yi) => sum + Math.pow(yi - yMean, 2), 0);

  return numerator / Math.sqrt(denominatorX * denominatorY);
};

function tTest(correlationCoefficient: number, sampleSize: number) {
  const alpha = 0.05;
  const degreesOfFreedom = sampleSize - 2;
  const tStatistic = (correlationCoefficient * Math.sqrt(sampleSize - 2)) / Math.sqrt(1 - correlationCoefficient ** 2);
  const criticalValue = jstat.studentt.inv(1 - alpha / 2, degreesOfFreedom);
  const isSignificant = Math.abs(tStatistic) > criticalValue;
  return {
    tStatistic,
    criticalValue,
    isSignificant
  };
}

function makeRegression(xValues: number[], yValues: number[]) {
  const xMean = calculateAverageValue(xValues);
  const yMean = calculateAverageValue(yValues);

  let numerator = 0;
  let denominator = 0;

  for (let i = 0; i < xValues.length; i++) {
    numerator += (xValues[i] - xMean) * (yValues[i] - yMean);
    denominator += (xValues[i] - xMean) ** 2;
  }

  const b = numerator / denominator;
  const a = yMean - b * xMean;
  return { a, b };
}

function regressionTTest(xValues: number[], yValues: number[], intercept: number, slope: number) {
  const residuals = yValues.map((actual, index) => actual - (intercept + slope * xValues[index]));
  const n = xValues.length;
  const k = 2;
  const mse = jstat.sum(residuals.map(residual => residual ** 2)) / (n - k);
  const seIntercept = Math.sqrt(mse * (1 / n + (xValues.reduce((acc, val) => acc + val ** 2, 0) / (n * jstat.sum(xValues) ** 2))));
  const seSlope = Math.sqrt(mse / (jstat.sum(xValues.map(val => val ** 2))));
  const tStatIntercept = intercept / seIntercept;
  const tStatSlope = slope / seSlope;
  return {
    tStatIntercept,
    tStatSlope,
    pValueSlope: 2 * (1 - jstat.studentt.cdf(Math.abs(tStatSlope), n - k)),
    pValueIntercept: 2 * (1 - jstat.studentt.cdf(Math.abs(tStatIntercept), n - k))
  };
}

function getPairs() {
  return [
    ['PM_1', 'temperature'],
    ['PM_1', 'humidity'],
    ['PM_1', 'wind_speed'],
    ['PM_1', 'datetime'],
    ['PM_2_5', 'temperature'],
    ['PM_2_5', 'humidity'],
    ['PM_2_5', 'wind_speed'],
    ['PM_2_5', 'datetime'],
    ['PM_10', 'temperature'],
    ['PM_10', 'humidity'],
    ['PM_10', 'wind_speed'],
    ['PM_10', 'datetime'],
    ['CO2', 'temperature'],
    ['CO2', 'humidity'],
    ['CO2', 'wind_speed'],
    ['CO2', 'datetime'],
  ];
}

app.get('/', (req: Request, res: Response) => {
  res.redirect('/statistics');
});

app.post('/add1', async (req, res) => {
  const weatherUri = 'https://api.weatherapi.com/v1/current.json?key=b2b6c5f60e5f45fba2d75302231005&q=49.9, 23.6&aqi=yes';
  const { data: weatherData } = await axios.get(weatherUri);
  const data = req.body;
  const temperature = data.temperature;
  const humidity = data.humidity;
  const direction = convertDirection(weatherData.current.wind_dir);
  const wind_speed = weatherData.current.wind_kph;

  const dust_1_0 = data.dust_1_0;
  const dust_2_5 = data.dust_2_5;
  const dust_10 = data.dust_10;
  const CO2 = data.CO2 ?? null;
  const deviceId = data.deviceId;

  const timestamp = Date.now() / 1000;
  const query = 'INSERT INTO test_measurements ' +
    '(dust_1_0, dust_2_5, dust_10, CO2, temperature, humidity, direction, wind_speed, deviceId, datetime) VALUES' +
    '(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';
  const measurements = [dust_1_0, dust_2_5, dust_10, CO2, temperature, humidity, direction, wind_speed, deviceId, timestamp];
  connection.query(query, measurements, (err: ErrorRequestHandler, rows: { solution: any; }[], fields: any) => {
    if (err) throw err;
    console.log(err);
    res.end();
  });
  res.send(data);
  res.sendStatus(200);
});

app.post('/add', async (req, res) => {

  const weatherUri = 'https://api.weatherapi.com/v1/current.json?key=b2b6c5f60e5f45fba2d75302231005&q=49.9, 23.6&aqi=yes';
  const { data: weatherData } = await axios.get(weatherUri);
  const data = req.body;
  const temperature = data.temperature;
  const humidity = data.humidity;
  const direction = convertDirection(weatherData.current.wind_dir);
  const wind_speed = weatherData.current.wind_kph;

  const dust_1_0 = data.dust_1_0;
  const dust_2_5 = data.dust_2_5;
  const dust_10 = data.dust_10;
  const CO2 = data.CO2 ?? null;
  const deviceId = data.deviceId;

  const timestamp = Date.now() / 1000;
  const query = 'INSERT INTO measurements ' +
    '(dust_1_0, dust_2_5, dust_10, CO2, temperature, humidity, direction, wind_speed, deviceId, datetime) VALUES' +
    '(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';
  const measurements = [dust_1_0, dust_2_5, dust_10, CO2, temperature, humidity, direction, wind_speed, deviceId, timestamp];
  connection.query(query, measurements, (err: ErrorRequestHandler, rows: { solution: any; }[], fields: any) => {
    if (err) throw err;
    console.log(err);
    res.end();
  });
  res.send(data);
  res.sendStatus(200);
});

app.get('/indicators', async (req, res) => {
  res.render('indicators');
});

app.get('/stat-indicators', cors(), async (req, res) => {
  const query = 'SELECT * FROM measurements';
  connection.query(query, (err: ErrorRequestHandler, rows: Measurement[], fields: any) => {
    if (err) throw err;
    const result = {
      averageValues: {},
      medianValues: {},
      varianceValues: {},
      standardDeviationValues: {},
      ks_test: {},
      critical_values: {}
    };
    const PM_1_values = rows.map(row => row.dust_1_0);
    const PM_2_5_values = rows.map(row => row.dust_2_5);
    const PM_10_values = rows.map(row => row.dust_10);
    const CO2_values = rows.map(row => row.CO2);
    const temperature_values = rows.map(row => row.temperature);
    const humidity_values = rows.map(row => row.humidity);
    const wind_speed_values = rows.map(row => row.wind_speed);

    result.averageValues = {
      pm_1: calculateAverageValue(PM_1_values),
      pm_2_5: calculateAverageValue(PM_2_5_values),
      pm_10: calculateAverageValue(PM_10_values),
      co2: calculateAverageValue(CO2_values),
      temperature: calculateAverageValue(temperature_values),
      humidity: calculateAverageValue(humidity_values),
      wind_speed: calculateAverageValue(wind_speed_values)
    };
    result.medianValues = {
      pm_1: calculateMedianValue(PM_1_values),
      pm_2_5: calculateMedianValue(PM_2_5_values),
      pm_10: calculateMedianValue(PM_10_values),
      co2: calculateMedianValue(CO2_values),
      temperature: calculateMedianValue(temperature_values),
      humidity: calculateMedianValue(humidity_values),
      wind_speed: calculateMedianValue(wind_speed_values)
    };
    result.varianceValues = {
      pm_1: calculateVariance(PM_1_values),
      pm_2_5: calculateVariance(PM_2_5_values),
      pm_10: calculateVariance(PM_10_values),
      co2: calculateVariance(CO2_values),
      temperature: calculateVariance(temperature_values),
      humidity: calculateVariance(humidity_values),
      wind_speed: calculateVariance(wind_speed_values)
    };
    result.standardDeviationValues = {
      pm_1: calculateSD(PM_1_values),
      pm_2_5: calculateSD(PM_2_5_values),
      pm_10: calculateSD(PM_10_values),
      co2: calculateSD(CO2_values),
      temperature: calculateSD(temperature_values),
      humidity: calculateSD(humidity_values),
      wind_speed: calculateSD(wind_speed_values)
    };
    result.ks_test = {
      pm_1: ksTest(PM_1_values),
      pm_2_5: ksTest(PM_2_5_values),
      pm_10: ksTest(PM_10_values),
      co2: ksTest(CO2_values),
      temperature: ksTest(temperature_values),
      humidity: ksTest(humidity_values),
      wind_speed: ksTest(wind_speed_values)
    };
    result.critical_values = {
      pm_1: calculateCritical(PM_1_values),
      pm_2_5: calculateCritical(PM_2_5_values),
      pm_10: calculateCritical(PM_10_values),
      co2: calculateCritical(CO2_values),
      temperature: calculateCritical(temperature_values),
      humidity: calculateCritical(humidity_values),
      wind_speed: calculateCritical(wind_speed_values)
    };
    res.end(JSON.stringify(result));
  });
});

app.get('/corelations', cors(), async (req, res) => {
  const query = 'SELECT * FROM measurements';
  connection.query(query, (err: ErrorRequestHandler, rows: Measurement[], fields: any) => {
    if (err) throw err;
    const filteredRows = clearData(rows);
    const data = {
      PM_1: filteredRows.map(row => row.dust_1_0),
      PM_2_5: filteredRows.map(row => row.dust_2_5),
      PM_10: filteredRows.map(row => row.dust_10),
      CO2: filteredRows.map(row => row.CO2),
      temperature: filteredRows.map(row => row.temperature),
      humidity: filteredRows.map(row => row.humidity),
      wind_speed: filteredRows.map(row => row.wind_speed),
      datetime: filteredRows.map(row => row.datetime)
    };
    const pairs = getPairs();
    const result: Record<string, any> = {};
    for (let i = 0; i < pairs.length; i++) {
      const [rowY, rowX] = pairs[i];
      const firstRow: number[] = data[rowX as keyof typeof data] as number[];
      const secondRow: number[] = data[rowY as keyof typeof data] as number[];
      const r = Number(findCorrelation(firstRow, secondRow).toFixed(5));
      const { tStatistic, criticalValue, isSignificant } = tTest(r, firstRow.length);
      const { a: regressionIntercept, b: regressionSlope } = makeRegression(firstRow, secondRow);
      const {
        pValueIntercept,
        pValueSlope,
        tStatIntercept,
        tStatSlope
      } = regressionTTest(firstRow, secondRow, regressionIntercept, regressionSlope);
      const passedTTest = ((pValueIntercept < 0.05) && (pValueSlope < 0.05)) ? 'accepted' : 'rejected';
      result[rowX + ' | ' + rowY] = {
        r,
        tStatistic,
        criticalValue,
        isSignificant,
        regressionIntercept,
        regressionSlope,
        tStatIntercept,
        tStatSlope,
        pValueIntercept,
        pValueSlope,
        passedTTest
      };
    }

    res.end(JSON.stringify(result));
  });
});

function convertDirection(original: string) {
  const mappingObj = {
    W: ['WSW', 'W', 'NWN', 'WNW'],
    N: ['NNW', 'N', 'NNE'],
    E: ['ENE', 'E', 'ESE'],
    S: ['SSE', 'S', 'SSW'],
    NW: ['NW'],
    NE: ['NE'],
    SW: ['SW'],
    SE: ['SE']
  };

  for (const prop in mappingObj) {
    if (((mappingObj as any)[prop]).includes(original)) {
      return prop;
    }
  }
}

app.get('/statistics', (req, res) => {
  res.render('statistics');
});

app.get('/get-statistics', cors(), (req, res) => {
  const query = 'SELECT * FROM measurements;';

  connection.query(query, (err: ErrorRequestHandler, rows: { solution: any; }[], fields: any) => {
    if (err) throw err;
    console.log(err);
    res.end(JSON.stringify(rows));
  });
});

app.on('close', () => {
  connection.end();
});
