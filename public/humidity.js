const countHumidityValues = (data) => {

  const dust1_0_Humidity = [];
  const dust2_5_Humidity = [];
  const dust10_Humidity = [];
  const CO2_Humidity = [];
  for (let i = 0; i < data.length; i++) {
    if (dust1_0_Humidity[data[i].humidity] === undefined) {
      dust1_0_Humidity[data[i].humidity] = [];
    }
    if (dust2_5_Humidity[data[i].humidity] === undefined) {
      dust2_5_Humidity[data[i].humidity] = [];
    }
    if (dust10_Humidity[data[i].humidity] === undefined) {
      dust10_Humidity[data[i].humidity] = [];
    }
    if (CO2_Humidity[data[i].humidity] === undefined) {
      CO2_Humidity[data[i].humidity] = [];
    }
    dust1_0_Humidity[data[i].humidity].push(data[i].dust_1_0);
    dust2_5_Humidity[data[i].humidity].push(data[i].dust_2_5);
    dust10_Humidity[data[i].humidity].push(data[i].dust_10);
    CO2_Humidity[data[i].humidity].push(data[i].CO2);
  }

  const dust_1_By_Humidity = dust1_0_Humidity.map(measurement => {
    return arraySum(measurement) / measurement.length;
  });
  const dust_2_5_By_Humidity = dust2_5_Humidity.map(measurement => {
    return arraySum(measurement) / measurement.length;
  });
  const dust_10_By_Humidity = dust10_Humidity.map(measurement => {
    return arraySum(measurement) / measurement.length;
  });
  const CO2_By_Humidity = CO2_Humidity.map(measurement => {
    return arraySum(measurement) / measurement.length;
  });

  const dust_hum = document.getElementById('dust_hum');
  const co2_hum = document.getElementById('co2_hum');

  new Chart(dust_hum, {
    type: 'line',
    data: {
      labels: Array.from(Array(100).keys()),
      datasets: [{
        label: 'Dust 1.0 Humidity Levels',
        data: dust_1_By_Humidity,
        borderWidth: 1,
        borderColor: '#ff0000',
      }, {
        label: 'Dust 2.5 Humidity Levels',
        data: dust_2_5_By_Humidity,
        borderWidth: 1,
        borderColor: '#00ff00'
      }, {
        label: 'Dust 10 Humidity Levels',
        data: dust_10_By_Humidity,
        borderWidth: 1,
        borderColor: '#0000ff',
      }]
    },
    options: {
      plugins: {
        title: {
          display: true,
          text: 'Dust by humidity level'
        }
      },
      scales: {
        y: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'Dust, µg/m3'
          }
        },
        x: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'Humidity, %'
          },
          min: 50
        }
      }
    }
  });

  new Chart(co2_hum, {
    type: 'line',
    data: {
      labels: Array.from(Array(100).keys()),
      datasets: [{
        label: 'CO2 Humidity Levels',
        data: CO2_By_Humidity,
        borderWidth: 1,
      }]
    },
    options: {
      plugins: {
        title: {
          display: true,
          text: 'CO2 by humidity level'
        }
      },
      scales: {
        y: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'CO2, ppm'
          }
        },
        x: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'Humidity, %'
          },
          min: 50
        }
      }
    }
  });
};
