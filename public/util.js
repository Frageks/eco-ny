const make2Digits = (number) => {
  return number < 10 ? '0' + number : number;
};

const getDates = (data) => {
  return data.map(row => {
    const date = new Date(row.datetime * 1000);
    const days = date.getDate();
    let month = date.getMonth() + 1;
    if (month < 10) month = '0' + month.toString();
    const secs = date.getSeconds();
    const mins = date.getMinutes();
    const hours = date.getHours();
    return `${days}.${month} ${hours}:${mins}:${secs}`;
  });
};

const arraySum = (array) => {
  let result = 0;
  for (let i = 0; i < array.length; i++) {
    result += array[i];
  }
  return result;
};

const windDirections = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];

const getMonth = (date) => {
  const monthNumber = date.getMonth();
  let month = monthNumber + 1;
  return (month < 10 ? '0' : '') + month;
};

const convertToDate = (timestamp) => {
  const date = new Date(timestamp * 1000);
  const year = date.getFullYear();
  const month = getMonth(date);
  const day = date.getDate();
  const hour = date.getHours();
  const mins = date.getMinutes();
  return `${year}.${month}.${day} ${hour}:${mins}`;
};
