const countOverallValues = (data) => {
  const dates = getDates(data);

  const CO2 = data.map((row) => row.CO2);
  const dust_1_0 = data.map((row) => row.dust_1_0);
  const dust_2_5 = data.map((row) => row.dust_2_5);
  const dust_10 = data.map((row) => row.dust_10);

  const processedDataHourly = data.reduce(
    (acc, row) => {
      const date = new Date((row.datetime + 30 * 60) * 1000);
      const hours = make2Digits(date.getHours());
      const day = make2Digits(date.getDate());
      const month = getMonth(date);
      const h = `${day}.${month} ${hours}:00`;
      if (acc.pm1[h] === undefined) {
        acc.pm1[h] = { value: 0, count: 0 };
      }
      if (acc.pm2_5[h] === undefined) {
        acc.pm2_5[h] = { value: 0, count: 0 };
      }
      if (acc.pm10[h] === undefined) {
        acc.pm10[h] = { value: 0, count: 0 };
      }
      if (acc.co2[h] === undefined) {
        acc.co2[h] = { value: 0, count: 0 };
      }
      acc.pm1[h].value += row.dust_1_0;
      acc.pm1[h].count++;
      acc.pm2_5[h].value += row.dust_2_5;
      acc.pm2_5[h].count++;
      acc.pm10[h].value += row.dust_10;
      acc.pm10[h].count++;
      acc.co2[h].value += row.CO2;
      acc.co2[h].count++;

      return acc;
    },
    {
      pm1: {},
      pm2_5: {},
      pm10: {},
      co2: {},
    }
  );

  for (let i = 0; i < Object.keys(processedDataHourly).length; i++) {
    const hours = processedDataHourly[Object.keys(processedDataHourly)[i]];
    for (let j = 0; j < Object.keys(hours).length; j++) {
      const hour = Object.keys(hours)[j];
      hours[hour] = hours[hour].value / hours[hour].count;
    }
  }

  const ctxCO2 = document.getElementById("myChart_CO2");
  const ctxPM1 = document.getElementById("myChart_PM1");
  const ctxPM2_5 = document.getElementById("myChart_PM2_5");
  const ctxPM10 = document.getElementById("myChart_PM10");

  new Chart(ctxCO2, {
    type: "line",
    data: {
      labels: dates,
      datasets: [
        {
          type: "line",
          label: "CO2",
          data: CO2,
          backgroundColor: "#ee0000",
          borderColor: "#ff0000",
          pointRadius: 0,
          pointHoverRadius: 1,
          borderWidth: 1,
        },
        {
          type: "line",
          label: "CO2 Hourly",
          order: 2,
          data: Object.values(processedDataHourly.co2),
          backgroundColor: "#3333ee",
          borderColor: "#3333ee",
          pointRadius: 1,
          pointHoverRadius: 1,
          borderWidth: 2,
          xAxisID: "xAxes",
        },
      ],
    },
    options: {
      maintainAspectRatio: false,
      responsive: true,
      title: {
        display: true,
        text: "Chart.js - Combo Chart With Multiple Scales (X Axis)",
      },
      scales: {
        x: {
          bounds: "data",
          position: "bottom",
          offset: false,
          display: false,
        },
        xAxes: {
          labels: Object.keys(processedDataHourly.co2),
          bounds: "data",
          axis: "x",
          position: "bottom",
          offset: false,
          display: false,
        },
        y: {
          ticks: {
            min: 0,
            max: 50,
            padding: 25
          },
          min: 350,
          max: 650
        },
      },
      plugins: {
        zoom: {
          zoom: {
            wheel: {
              enabled: true,
            },
            pinch: {
              enabled: true,
            },
            mode: "x",
          },
        },
      },
    },
  });

  new Chart(ctxPM1, {
    type: "line",
    data: {
      labels: dates,
      datasets: [
        {
          type: "line",
          backgroundColor: "#ee0000",
          borderColor: "#ff0000",
          pointRadius: 0,
          pointHoverRadius: 1,
          borderWidth: 1,
          label: "Dust 1.0",
          data: dust_1_0,
        },
        {
          type: 'line',
          backgroundColor: '#3333ee',
          borderColor: '#3333ee',
          order: 2,
          xAxisID: 'xAxes',
          label: "Average hour level of PM1.0 dust",
          data: Object.values(processedDataHourly.pm1),
          pointRadius: 1,
          pointHoverRadius: 1,
          borderWidth: 2,
        },
      ],
    },
    options: {
      maintainAspectRatio: false,
      responsive: true,
      title: {
        display: true,
        text: "Chart.js - Combo Chart With Multiple Scales (X Axis)",
      },
      scales: {
        x: {
          bounds: "data",
          position: "bottom",
          offset: false,
          display: false
        },
        xAxes: {
          labels: Object.keys(processedDataHourly.pm1),
          bounds: "data",
          axis: "x",
          position: "bottom",
          offset: false,
          display: false,
        },
        y: {
          ticks: {
            min: 0,
            max: 50,
            padding: 25
          },
          min: 0,
          max: 140
        },
      },
      plugins: {
        zoom: {
          zoom: {
            wheel: {
              enabled: true,
            },
            pinch: {
              enabled: true,
            },
            mode: "x",
          },
        },
      },
    },
  });

  new Chart(ctxPM2_5, {
    type: "line",
    data: {
      labels: dates,
      datasets: [
        {
          type: "line",
          backgroundColor: "#ee0000",
          label: "Dust 2.5",
          data: dust_2_5,
          pointRadius: 0,
          pointHoverRadius: 1,
          borderWidth: 1,
          borderColor: "#ff0000",
        },
        {
          type: 'line',
          backgroundColor: '#3333ee',
          borderColor: '#3333ee',
          order: 2,
          xAxisID: 'xAxes',
          label: "Average hour level of PM2.5 dust",
          data: Object.values(processedDataHourly.pm2_5),
          pointRadius: 1,
          pointHoverRadius: 1,
          borderWidth: 2,
        },
      ],
    },
    options: {
      maintainAspectRatio: false,
      responsive: true,
      title: {
        display: true,
        text: "Chart.js - Combo Chart With Multiple Scales (X Axis)",
      },
      scales: {
        x: {
          bounds: "data",
          position: "bottom",
          offset: false,
          display: false,
        },
        xAxes: {
          labels: Object.keys(processedDataHourly.pm2_5),
          bounds: "data",
          axis: "x",
          position: "bottom",
          offset: false,
          display: false,
        },
        y: {
          ticks: {
            min: 0,
            max: 50,
            padding: 25
          },
          min: 0,
          max: 250
        },
      },
      plugins: {
        zoom: {
          zoom: {
            wheel: {
              enabled: true,
            },
            pinch: {
              enabled: true,
            },
            mode: "x",
          },
        },
      },
    },
  });

  new Chart(ctxPM10, {
    type: "line",
    data: {
      labels: dates,
      datasets: [
        {
          type: "line",
          backgroundColor: "#ee0000",
          label: "Dust 10",
          data: dust_10,
          pointRadius: 0,
          pointHoverRadius: 1,
          borderWidth: 1,
          borderColor: "#ff0000",
        },
        {
          type: 'line',
          backgroundColor: '#3333ee',
          borderColor: '#3333ee',
          xAxisID: 'xAxes',
          order: 2,
          label: "Average hour level of PM10 dust",
          data: Object.values(processedDataHourly.pm10),
          pointRadius: 1,
          pointHoverRadius: 1,
          borderWidth: 2,
        },
      ],
    },
    options: {
      maintainAspectRatio: false,
      responsive: true,
      title: {
        display: true,
        text: "Chart.js - Combo Chart With Multiple Scales (X Axis)",
      },
      scales: {
        x: {
          bounds: "data",
          position: "bottom",
          offset: false,
          display: false,
        },
        xAxes: {
          labels: Object.keys(processedDataHourly.pm10),
          bounds: "data",
          axis: "x",
          position: "bottom",
          offset: false,
          display: false,
        },
        y: {
          ticks: {
            min: 0,
            max: 50,
            padding: 25
          },
          min: 0,
          max: 300
        },
      },
      plugins: {
        zoom: {
          zoom: {
            wheel: {
              enabled: true,
            },
            pinch: {
              enabled: true,
            },
            mode: "x",
          },
        },
      },
    },
  });
};
