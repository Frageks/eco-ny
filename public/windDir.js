const getDustData = (data) => {
  const mapped = data.reduce((acc, row) => {
      const windSpeedRoundedValue = Math.floor(row.wind_speed + 0.5);
      const existing = acc.get(windSpeedRoundedValue) ?? [];
      existing.push({
        pm1: row.dust_1_0,
        pm2_5: row.dust_2_5,
        pm10: row.dust_10,
        co2: row.CO2
      });
      acc.set(windSpeedRoundedValue, existing);
      return acc;
  }, new Map());

  const keyList = [...mapped.keys()].map(k => Number(k));
  keyList.sort((a, b) => a - b);
  const res = { keys: keyList, pm1: [], pm2_5: [], pm10: [], co2: [] };
  for (let i = 0; i < keyList.length; i++) {
    const pm1els = mapped.get(keyList[i]).map(row => row.pm1);
    res.pm1.push(pm1els.reduce((a, b) => a + b, 0) / pm1els.length);

    const pm2_5els = mapped.get(keyList[i]).map(row => row.pm2_5);
    res.pm2_5.push(pm2_5els.reduce((a, b) => a + b, 0) / pm2_5els.length);

    const pm10els = mapped.get(keyList[i]).map(row => row.pm10);
    res.pm10.push(pm10els.reduce((a, b) => a + b, 0) / pm10els.length);

    const co2els = mapped.get(keyList[i]).map(row => row.co2);
    res.co2.push(co2els.reduce((a, b) => a + b, 0) / co2els.length);
  }

  return res;
};

const dustByDir = (dir, data) => {
  const allDataByDir = data.filter((row) => row.direction === dir);
  const res = allDataByDir.map((row) => ({
    dust1_0: row.dust_1_0,
    dust2_5: row.dust_2_5,
    dust10: row.dust_10,
    co2: row.CO2,
    speed: row.wind_speed,
  }));
  const windSpeedArray = res.map((row) => row.speed);
  const minSpeed = Math.min(...windSpeedArray);
  const maxSpeed = Math.max(...windSpeedArray);
  const labels = [];
  for (let i = minSpeed; i < maxSpeed; i += 2) {
    labels.push(i);
  }
  const findIndex = (num) => {
    for (let i = 0; i < labels.length; i++) {
      if (
        num >= labels[i] &&
        (num < labels[i + 1] || labels[i + 1] === undefined)
      ) {
        return i;
      }
    }
  };

  const values = {
    dust1_0: [],
    dust2_5: [],
    dust10: [],
    co2: [],
  };
  const result = {
    labels,
    values: {
      dust1_0: [],
      dust2_5: [],
      dust10: [],
      co2: [],
    },
  };
  for (let i = 0; i < res.length; i++) {
    const index = findIndex(res[i].speed);
    if (values.dust1_0[index] === undefined) {
      values.dust1_0[index] = [];
    }
    if (values.dust2_5[index] === undefined) {
      values.dust2_5[index] = [];
    }
    if (values.dust10[index] === undefined) {
      values.dust10[index] = [];
    }
    if (values.co2[index] === undefined) {
      values.co2[index] = [];
    }
    values.dust1_0[index].push(res[i].dust1_0);
    values.dust2_5[index].push(res[i].dust2_5);
    values.dust10[index].push(res[i].dust10);
    values.co2[index].push(res[i].co2);
  }
  for (let i = 0; i < values.dust1_0.length; i++) {
    const value = values.dust1_0[i];
    if (value !== undefined) {
      result.values.dust1_0[i] = arraySum(value) / value.length;
    }
  }
  for (let i = 0; i < values.dust2_5.length; i++) {
    const value = values.dust2_5[i];
    if (value !== undefined) {
      result.values.dust2_5[i] = arraySum(value ?? []) / value.length;
    }
  }
  for (let i = 0; i < values.dust10.length; i++) {
    const value = values.dust10[i];
    if (value !== undefined) {
      result.values.dust10[i] = arraySum(value ?? []) / value.length;
    }
  }
  for (let i = 0; i < values.co2.length; i++) {
    const value = values.co2[i];
    if (value !== undefined) {
      result.values.co2[i] = arraySum(value ?? []) / value.length;
    }
  }
  return result;
};

const countWindDirValues = (data) => {
  const grouppedDataByWind_Dir = data.reduce((acc, curr) => {
    const index = windDirections.indexOf(curr.direction);
    if (acc.pm1 === undefined) {
      acc.pm1 = [];
    }
    if (acc.pm2_5 === undefined) {
      acc.pm2_5 = [];
    }
    if (acc.pm10 === undefined) {
      acc.pm10 = [];
    }
    if (acc.co2 === undefined) {
      acc.co2 = [];
    }
    if (acc.pm1[index] === undefined) {
      acc.pm1[index] = [];
    }
    if (acc.pm2_5[index] === undefined) {
      acc.pm2_5[index] = [];
    }
    if (acc.pm10[index] === undefined) {
      acc.pm10[index] = [];
    }
    if (acc.co2[index] === undefined) {
      acc.co2[index] = [];
    }
    acc.pm1[index].push(curr.dust_1_0);
    acc.pm2_5[index].push(curr.dust_2_5);
    acc.pm10[index].push(curr.dust_10);
    acc.co2[index].push(curr.CO2);
    return acc;
  }, []);

  const dust_1_By_Wind_Dir = grouppedDataByWind_Dir.pm1.map((measurement) => {
    return arraySum(measurement) / measurement.length;
  });
  const dust_2_5_By_Wind_Dir = grouppedDataByWind_Dir.pm2_5.map(
    (measurement) => {
      return arraySum(measurement) / measurement.length;
    }
  );
  const dust_10_By_Wind_Dir = grouppedDataByWind_Dir.pm10.map((measurement) => {
    return arraySum(measurement) / measurement.length;
  });
  const CO2_By_Wind_Dir = grouppedDataByWind_Dir.co2.map((measurement) => {
    return arraySum(measurement) / measurement.length;
  });
  const dust_wnd = document.getElementById("dust_wnd");
  const co2_wnd = document.getElementById("co2_wnd");
  const dust_west = document.getElementById("dust_west");
  const dust_north = document.getElementById("dust_north");
  const dust_east = document.getElementById("dust_east");
  const dust_south = document.getElementById("dust_south");

  const westDust = dustByDir("W", data);
  const northDust = dustByDir("N", data);
  const eastDust = dustByDir("E", data);
  const southDust = dustByDir("S", data);

  const dustData = getDustData(data);

  new Chart(dust_wnd, {
    type: "radar",
    data: {
      labels: windDirections,
      datasets: [
        {
          label: "Dust 1.0, µg/m3",
          data: dust_1_By_Wind_Dir,
          borderWidth: 1,
        },
        {
          label: "Dust 2.5, µg/m3",
          data: dust_2_5_By_Wind_Dir,
          borderWidth: 1,
        },
        {
          label: "Dust 10, µg/m3",
          data: dust_10_By_Wind_Dir,
          borderWidth: 1,
        },
      ],
    },
    options: {
      plugins: {
        title: {
          display: true,
          text: "Dust by wind direction",
        },
      },
    },
  });

  new Chart(co2_wnd, {
    type: "radar",
    data: {
      labels: windDirections,
      datasets: [
        {
          label: "CO2, ppm",
          data: CO2_By_Wind_Dir,
          borderWidth: 1,
        },
      ],
    },
    options: {
      plugins: {
        title: {
          display: true,
          text: "CO2 by wind direction",
        },
      },
    },
  });

  new Chart(dust_west, {
    type: "line",
    data: {
      labels: westDust.labels,
      datasets: [
        {
          label: "Dust 1.0",
          data: westDust.values.dust1_0,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "Dust 2.5",
          data: westDust.values.dust2_5,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "Dust 10",
          data: westDust.values.dust10,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "CO2",
          data: westDust.values.co2,
          borderWidth: 1,
          yAxisID: "y1",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
      ],
    },
    options: {
      responsive: true,
      interaction: {
        mode: "index",
        intersect: false,
      },
      stacked: false,
      plugins: {
        title: {
          display: true,
          text: "Pollution by wind direction and speed: WEST",
        },
      },
      scales: {
        y: {
          type: "linear",
          display: true,
          position: "left",
          title: {
            display: true,
            text: "Dust, µg/m3",
          },
        },
        y1: {
          type: "linear",
          display: true,
          position: "right",

          // grid line settings
          grid: {
            drawOnChartArea: false, // only want the grid lines for one axis to show up
          },
          title: {
            display: true,
            text: "CO2, ppm",
          },
        },
        x: {
          title: {
            display: true,
            text: "Wind speed, m/s",
          },
        },
      },
    },
  });

  new Chart(dust_north, {
    type: "line",
    data: {
      labels: northDust.labels,
      datasets: [
        {
          label: "Dust 1.0",
          data: northDust.values.dust1_0,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "Dust 2.5",
          data: northDust.values.dust2_5,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "Dust 10",
          data: northDust.values.dust10,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "CO2",
          data: northDust.values.co2,
          borderWidth: 1,
          yAxisID: "y1",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
      ],
    },
    options: {
      responsive: true,
      interaction: {
        mode: "index",
        intersect: false,
      },
      stacked: false,
      plugins: {
        title: {
          display: true,
          text: "Pollution by wind direction and speed: NORTH",
        },
      },
      scales: {
        y: {
          type: "linear",
          display: true,
          position: "left",
          title: {
            display: true,
            text: "Dust, µg/m3",
          },
        },
        y1: {
          type: "linear",
          display: true,
          position: "right",

          // grid line settings
          grid: {
            drawOnChartArea: false, // only want the grid lines for one axis to show up
          },
          title: {
            display: true,
            text: "CO2, ppm",
          },
        },
        x: {
          title: {
            display: true,
            text: "Wind speed, m/s",
          },
        },
      },
    },
  });

  new Chart(dust_east, {
    type: "line",
    data: {
      labels: eastDust.labels,
      datasets: [
        {
          label: "Dust 1.0",
          data: eastDust.values.dust1_0,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "Dust 2.5",
          data: eastDust.values.dust2_5,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "Dust 10",
          data: eastDust.values.dust10,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "CO2",
          data: eastDust.values.co2,
          borderWidth: 1,
          yAxisID: "y1",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
      ],
    },
    options: {
      responsive: true,
      interaction: {
        mode: "index",
        intersect: false,
      },
      stacked: false,
      plugins: {
        title: {
          display: true,
          text: "Pollution by wind direction and speed: EAST",
        },
      },
      scales: {
        y: {
          type: "linear",
          display: true,
          position: "left",
          title: {
            display: true,
            text: "Dust, µg/m3",
          },
        },
        y1: {
          type: "linear",
          display: true,
          position: "right",

          // grid line settings
          grid: {
            drawOnChartArea: false, // only want the grid lines for one axis to show up
          },
          title: {
            display: true,
            text: "CO2, ppm",
          },
        },
        x: {
          title: {
            display: true,
            text: "Wind speed, m/s",
          },
        },
      },
    },
  });

  new Chart(dust_south, {
    type: "line",
    data: {
      labels: southDust.labels,
      datasets: [
        {
          label: "Dust 1.0",
          data: southDust.values.dust1_0,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "Dust 2.5",
          data: southDust.values.dust2_5,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "Dust 10",
          data: southDust.values.dust10,
          borderWidth: 1,
          yAxisID: "y",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
        {
          label: "CO2",
          data: southDust.values.co2,
          borderWidth: 1,
          yAxisID: "y1",
          trendlineLinear: {
            lineStyle: "line",
            width: 1,
          },
        },
      ],
    },
    options: {
      responsive: true,
      interaction: {
        mode: "index",
        intersect: false,
      },
      stacked: false,
      plugins: {
        title: {
          display: true,
          text: "Pollution by wind direction and speed: SOUTH",
        },
      },
      scales: {
        y: {
          type: "linear",
          display: true,
          position: "left",
          title: {
            display: true,
            text: "Dust, µg/m3",
          },
        },
        y1: {
          type: "linear",
          display: true,
          position: "right",

          // grid line settings
          grid: {
            drawOnChartArea: false, // only want the grid lines for one axis to show up
          },
          title: {
            display: true,
            text: "CO2, ppm",
          },
        },
        x: {
          title: {
            display: true,
            text: "Wind speed, m/s",
          },
        },
      },
    },
  });

  new Chart(document.getElementById("all_by_wind_speed"), {
    type: "line",
    data: {
      labels: dustData.keys,
      datasets: [
        {
          type: "line",
          label: "PM1.0",
          data: dustData.pm1,
          backgroundColor: "#ff5500",
          borderColor: "#ff5500",
          borderWidth: 1,
        },
        {
          type: "line",
          label: "PM2.5",
          data: dustData.pm2_5,
          backgroundColor: "#af0041",
          borderColor: "#af0041",
          borderWidth: 1,
        },
        {
          type: "line",
          label: "PM10",
          data: dustData.pm10,
          backgroundColor: "#ea0088",
          borderColor: "#ea0088",
          borderWidth: 1,
        },
        {
          type: "line",
          order: 2,
          label: "CO2",
          data: dustData.co2,
          backgroundColor: "#3333ee",
          borderColor: "#3333ee",
          borderWidth: 1,
          yAxisID: "B",
        },
      ],
    },
    options: {
      scales: {
        y: {
          id: "A",
          position: "left",
          title: {
            display: true,
            text: "Dust concentration, µg/m3",
          },
        },
        B: {
          id: "B",
          position: "right",
          title: {
            display: true,
            text: "CO2 concentration, ppm",
          },
        },
        x: {
          title: {
            display: true,
            text: "Wind speed, m/s",
          },
        },
      },
    },
  });
};
