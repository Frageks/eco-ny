const countHourlyValues = (data) => {
  const dust_hourly = data.reduce((acc, curr) => {
    const date = new Date(curr.datetime * 1000);
    const hours = date.getHours();
    if (acc.pm1 === undefined) {
      acc.pm1 = [];
    }
    if (acc.pm2_5 === undefined) {
      acc.pm2_5 = [];
    }
    if (acc.pm10 === undefined) {
      acc.pm10 = [];
    }
    if (acc.co2 === undefined) {
      acc.co2 = [];
    }
    if (acc.pm1[hours] === undefined) {
      acc.pm1[hours] = [];
    }
    if (acc.pm2_5[hours] === undefined) {
      acc.pm2_5[hours] = [];
    }
    if (acc.pm10[hours] === undefined) {
      acc.pm10[hours] = [];
    }
    if (acc.co2[hours] === undefined) {
      acc.co2[hours] = [];
    }
    acc.pm1[hours].push(curr.dust_1_0);
    acc.pm2_5[hours].push(curr.dust_2_5);
    acc.pm10[hours].push(curr.dust_10);
    acc.co2[hours].push(curr.CO2);
    return acc;
  }, {});

  const avgHourDust_1_0 = dust_hourly.pm1.map(hourMeasurements => {
    return arraySum(hourMeasurements) / hourMeasurements.length;
  });
  const avgHourDust_2_5 = dust_hourly.pm2_5.map(hourMeasurements => {
    return arraySum(hourMeasurements) / hourMeasurements.length;
  });
  const avgHourDust_10 = dust_hourly.pm10.map(hourMeasurements => {
    return arraySum(hourMeasurements) / hourMeasurements.length;
  });
  const avgHourCo2 = dust_hourly.co2.map(hourMeasurements => {
    return arraySum(hourMeasurements) / hourMeasurements.length;
  });

  const dust_hourly_block = document.getElementById('dust_hourly');
  const co2_hourly_block = document.getElementById('co2_hourly');

  new Chart(dust_hourly_block, {
    type: 'line',
    data: {
      labels: Array.from(Array(24).keys()),
      datasets: [{
        label: 'dust_1_hourly',
        data: avgHourDust_1_0,
        borderWidth: 1,
        borderColor: '#ff0000'
      }, {
        label: 'dust_2_5_hourly',
        data: avgHourDust_2_5,
        borderWidth: 1,
        borderColor: '#00ff00'
      }, {
        label: 'dust_10_hourly',
        data: avgHourDust_10,
        borderWidth: 1,
        borderColor: '#0000ff'
      }]
    },
    options: {
      plugins: {
        title: {
          display: true,
          text: 'Dust by average hour'
        }
      },
      scales: {
        y: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'Dust, µg/m3'
          }
        },
        x: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'Hours of date'
          }
        }
      }
    }
  });

  new Chart(co2_hourly_block, {
    type: 'line',
    data: {
      labels: Array.from(Array(24).keys()),
      datasets: [{
        label: 'co2_hourly',
        data: avgHourCo2,
        borderWidth: 1
      }]
    },
    options: {
      plugins: {
        title: {
          display: true,
          text: 'CO2 by average hour'
        }
      },
      scales: {
        y: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'CO2, ppm'
          }
        },
        x: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'Hours of date'
          }
        }
      }
    }
  });
}