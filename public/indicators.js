const loadIndicatorsData = async () => {
  const response = await fetch("http://www.eco-ny.uno/stat-indicators");
  return await response.json();
}

const loadCorelationsData = async () => {
  const response = await fetch("http://www.eco-ny.uno/corelations");
  return await response.json();
}

const setTextCellText = (selector, value) => {
  const $el = document.querySelector(selector);
  $el.innerText = value;
}

function fillIndicatorsTable(data) {
  setTextCellText('.pm_1_values .mean', data.averageValues.pm_1);
  setTextCellText('.pm_2_5_values .mean', data.averageValues.pm_2_5);
  setTextCellText('.pm_10_values .mean', data.averageValues.pm_10);
  setTextCellText('.co2_values .mean', data.averageValues.co2);
  setTextCellText('.temperatures_values .mean', data.averageValues.temperature);
  setTextCellText('.humidity_values .mean', data.averageValues.humidity);
  setTextCellText('.wind_speed_values .mean', data.averageValues.wind_speed);

  setTextCellText('.pm_1_values .median', data.medianValues.pm_1);
  setTextCellText('.pm_2_5_values .median', data.medianValues.pm_2_5);
  setTextCellText('.pm_10_values .median', data.medianValues.pm_10);
  setTextCellText('.co2_values .median', data.medianValues.co2);
  setTextCellText('.temperatures_values .median', data.medianValues.temperature);
  setTextCellText('.humidity_values .median', data.medianValues.humidity);
  setTextCellText('.wind_speed_values .median', data.medianValues.wind_speed);

  setTextCellText('.pm_1_values .deviance', data.varianceValues.pm_1);
  setTextCellText('.pm_2_5_values .deviance', data.varianceValues.pm_2_5);
  setTextCellText('.pm_10_values .deviance', data.varianceValues.pm_10);
  setTextCellText('.co2_values .deviance', data.varianceValues.co2);
  setTextCellText('.temperatures_values .deviance', data.varianceValues.temperature);
  setTextCellText('.humidity_values .deviance', data.varianceValues.humidity);
  setTextCellText('.wind_speed_values .deviance', data.varianceValues.wind_speed);

  setTextCellText('.pm_1_values .sd', data.standardDeviationValues.pm_1);
  setTextCellText('.pm_2_5_values .sd', data.standardDeviationValues.pm_2_5);
  setTextCellText('.pm_10_values .sd', data.standardDeviationValues.pm_10);
  setTextCellText('.co2_values .sd', data.standardDeviationValues.co2);
  setTextCellText('.temperatures_values .sd', data.standardDeviationValues.temperature);
  setTextCellText('.humidity_values .sd', data.standardDeviationValues.humidity);
  setTextCellText('.wind_speed_values .sd', data.standardDeviationValues.wind_speed);

  setTextCellText('.pm_1_values .ks', data.ks_test.pm_1);
  setTextCellText('.pm_2_5_values .ks', data.ks_test.pm_2_5);
  setTextCellText('.pm_10_values .ks', data.ks_test.pm_10);
  setTextCellText('.co2_values .ks', data.ks_test.co2);
  setTextCellText('.temperatures_values .ks', data.ks_test.temperature);
  setTextCellText('.humidity_values .ks', data.ks_test.humidity);
  setTextCellText('.wind_speed_values .ks', data.ks_test.wind_speed);

  setTextCellText('.pm_1_values .critical', data.critical_values.pm_1);
  setTextCellText('.pm_2_5_values .critical', data.critical_values.pm_2_5);
  setTextCellText('.pm_10_values .critical', data.critical_values.pm_10);
  setTextCellText('.co2_values .critical', data.critical_values.co2);
  setTextCellText('.temperatures_values .critical', data.critical_values.temperature);
  setTextCellText('.humidity_values .critical', data.critical_values.humidity);
  setTextCellText('.wind_speed_values .critical', data.critical_values.wind_speed);

  setTextCellText('.pm_1_values .passed', data.ks_test.pm_1 <= data.critical_values.pm_1 ? 'rejected' : 'accepted');
  setTextCellText('.pm_2_5_values .passed', data.ks_test.pm_2_5 <= data.critical_values.pm_2_5 ? 'rejected' : 'accepted');
  setTextCellText('.pm_10_values .passed', data.ks_test.pm_10 <= data.critical_values.pm_10 ? 'rejected' : 'accepted');
  setTextCellText('.co2_values .passed', data.ks_test.co2 <= data.critical_values.co2 ? 'rejected' : 'accepted');
  setTextCellText('.temperatures_values .passed', data.ks_test.temperature <= data.critical_values.temperature ? 'rejected' : 'accepted');
  setTextCellText('.humidity_values .passed', data.ks_test.humidity <= data.critical_values.humidity ? 'rejected' : 'accepted');
  setTextCellText('.wind_speed_values .passed', data.ks_test.wind_speed <= data.critical_values.wind_speed ? 'rejected' : 'accepted');
}

async function setIndicatorsTable() {
  const indicatorsData = await loadIndicatorsData();
  fillIndicatorsTable(indicatorsData);
}

async function setCorelationsTable() {
  const corelationsData = await loadCorelationsData();
  const $table = document.getElementById('corelation-regression');
  const keys = Object.keys(corelationsData);
  for (let i = 0; i < keys.length; i++) {
    const $row = document.createElement('TR');
    const $firstCell = document.createElement('TD');
    $firstCell.innerText = keys[i];
    $row.appendChild($firstCell);
    const cellValue = corelationsData[keys[i]];
    const rowKeys = Object.keys(cellValue);
    for (let j = 0; j < rowKeys.length; j++) {
      const $cell = document.createElement('TD');
      $cell.innerText = typeof cellValue[rowKeys[j]] === 'number' ? cellValue[rowKeys[j]].toFixed(3) : cellValue[rowKeys[j]];
      $row.appendChild($cell);
    }
    $table.appendChild($row);
  }
}

document.addEventListener('DOMContentLoaded', async function() {
  setIndicatorsTable();
  setCorelationsTable();
}, false);
