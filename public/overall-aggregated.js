const countAggregatedValues = (data) => {
    const processedDataHourly =  data.reduce((acc, row) => {
        const date = new Date((row.datetime + 30 * 60) * 1000);
        const hours = date.getHours();
        const day = date.getDate();
        const month = getMonth(date);
        const year = date.getFullYear();
        const h = `${year}.${month}.${day} ${hours}`;
        if (acc.pm1[h] === undefined) {
            acc.pm1[h] = { value: 0, count: 0, };
        }
        if (acc.pm2_5[h] === undefined) {
            acc.pm2_5[h] = { value: 0, count: 0, };
        }
        if (acc.pm10[h] === undefined) {
            acc.pm10[h] = { value: 0, count: 0, };
        }
        if (acc.co2[h] === undefined) {
            acc.co2[h] = { value: 0, count: 0, };
        }
        acc.pm1[h].value += row.dust_1_0;
        acc.pm1[h].count++;
        acc.pm2_5[h].value += row.dust_2_5;
        acc.pm2_5[h].count++;
        acc.pm10[h].value += row.dust_10;
        acc.pm10[h].count++;
        acc.co2[h].value += row.CO2;
        acc.co2[h].count++;

        return acc;
    }, {
        pm1: {},
        pm2_5: {},
        pm10: {},
        co2: {}
    });

    for (let i = 0; i < Object.keys(processedDataHourly).length; i++) {
        const hours = processedDataHourly[Object.keys(processedDataHourly)[i]];
        for (let j = 0; j < Object.keys(hours).length; j++) {
            const hour = Object.keys(hours)[j];
            hours[hour] = hours[hour].value / hours[hour].count;
        }
    }
    new Chart(document.getElementById("co2Hourly"), {
        type: 'line',
        data: {
            labels: Object.keys(processedDataHourly.co2),
            datasets: [
                {
                    label: 'Average hour level of  CO2',
                    data: Object.values(processedDataHourly.co2),
                    borderWidth: 1,
                },
            ],
        },
    });

    new Chart(document.getElementById("pm1Hourly"), {
        type: 'line',
        data: {
            labels: Object.keys(processedDataHourly.pm1),
            datasets: [
                {
                    label: 'Average hour level of PM1.0',
                    data: Object.values(processedDataHourly.pm1),
                    borderWidth: 1,
                },
            ],
        },
    });

    new Chart(document.getElementById("pm2_5Hourly"), {
        type: 'line',
        data: {
            labels: Object.keys(processedDataHourly.pm2_5),
            datasets: [
                {
                    label: 'Average hour level of PM2.5',
                    data: Object.values(processedDataHourly.pm2_5),
                    borderWidth: 1,
                },
            ],
        },
    });

    new Chart(document.getElementById("pm10Hourly"), {
        type: 'line',
        data: {
            labels: Object.keys(processedDataHourly.pm10),
            datasets: [
                {
                    label: 'Average hour level of PM10',
                    data: Object.values(processedDataHourly.pm10),
                    borderWidth: 1,
                },
            ],
        },
    });
};
