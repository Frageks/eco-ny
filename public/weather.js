const countWeatherValues = (data) => {
    const processedWeatherData = data.map((row) => ({
        temp: row.temperature,
        humidity: row.humidity,
        windSpeed: row.wind_speed,
        time: convertToDate(row.datetime)
    }));

    new Chart(document.getElementById("tempToTime"), {
        type: 'line',
        data: {
            labels: processedWeatherData.map(row => row.time),
            datasets: [
                {
                    label: 'Temperature levels',
                    data: processedWeatherData.map(row => row.temp),
                    borderWidth: 1,
                    pointRadius: 1,
                },
            ],
        },
    });

    new Chart(document.getElementById("humidityToTime"), {
        type: 'line',
        data: {
            labels: processedWeatherData.map(row => row.time),
            datasets: [
                {
                    label: 'Humidity',
                    data: processedWeatherData.map(row => row.humidity),
                    borderWidth: 1,
                    pointRadius: 1,
                },
            ],
        },
    });

    new Chart(document.getElementById("windSpeedToTime"), {
        type: 'line',
        data: {
            labels: processedWeatherData.map(row => row.time),
            datasets: [
                {
                    label: 'Wind Speed',
                    data: processedWeatherData.map(row => row.windSpeed),
                    borderWidth: 1,
                    pointRadius: 1,
                },
            ],
        },
    });
};
