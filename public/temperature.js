const countTemperatureValues = (data) => {
  const temperatures = data.map(row => row.temperature);
  const minTemp = Math.min(...temperatures);
  const maxTemp = Math.max(...temperatures);
  const temps = [];
  for (let i = minTemp; i < maxTemp; i += 0.5) {
    temps.push(Number(i.toFixed(1)));
  }
  const findIndex = (num) => {
    for (let i = 0; i < temps.length; i++) {
      if (num >= temps[i] && (num < temps[i + 1] || temps[i + 1] === undefined)) {
        return i;
      }
    }
  };
  const grouppedByTemp1_0 = [];
  const grouppedByTemp2_5 = [];
  const grouppedByTemp10 = [];
  const grouppedByTempCO2 = [];
  for (let i = 0; i < data.length; i++) {
    const index = findIndex(data[i].temperature);
    if (grouppedByTemp1_0[index] === undefined) {
      grouppedByTemp1_0[index] = [];
    }
    if (grouppedByTemp2_5[index] === undefined) {
      grouppedByTemp2_5[index] = [];
    }
    if (grouppedByTemp10[index] === undefined) {
      grouppedByTemp10[index] = [];
    }
    if (grouppedByTempCO2[index] === undefined) {
      grouppedByTempCO2[index] = [];
    }
    grouppedByTemp1_0[index].push(data[i].dust_1_0);
    grouppedByTemp2_5[index].push(data[i].dust_2_5);
    grouppedByTemp10[index].push(data[i].dust_10);
    grouppedByTempCO2[index].push(data[i].CO2);
  }
  const dust1_0ToTemp = grouppedByTemp1_0.map(dustGroup => arraySum(dustGroup) / dustGroup.length);
  const dust2_5ToTemp = grouppedByTemp2_5.map(dustGroup => arraySum(dustGroup) / dustGroup.length);
  const dust10ToTemp = grouppedByTemp10.map(dustGroup => arraySum(dustGroup) / dustGroup.length);
  const co2ToTemp = grouppedByTempCO2.map(dustGroup => arraySum(dustGroup) / dustGroup.length);

  const dust_temp = document.getElementById('dust_temp');
  const co2_temp = document.getElementById('co2_temp');

  new Chart(dust_temp, {
    type: 'line',
    data: {
      labels: temps,
      datasets: [{
        label: 'Dust 1.0',
        data: dust1_0ToTemp,
        borderWidth: 1,
        borderColor: '#ff0000',
        trendlineLinear: {
          style: "#ff0000",
          lineStyle: "line",
          width: 1
        }
      }, {
        label: 'Dust 2.5',
        data: dust2_5ToTemp,
        borderWidth: 1,
        borderColor: '#00ff00',
        trendlineLinear: {
          style: "#00ff00",
          lineStyle: "line",
          width: 1
        }
      }, {
        label: 'Dust 10',
        data: dust10ToTemp,
        borderWidth: 1,
        borderColor: '#0000ff',
        trendlineLinear: {
          style: "#0000ff",
          lineStyle: "line",
          width: 1
        }
      }]
    },
    options: {
      plugins: {
        title: {
          display: true,
          text: 'Dust by Temperature levels'
        }
      },
      scales: {
        y: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'Dust, µg/m3'
          }
        },
        x: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'Temperature, C'
          }
        }
      }
    }
  });

  new Chart(co2_temp, {
    type: 'line',
    data: {
      labels: temps,
      datasets: [{
        label: 'CO2',
        data: co2ToTemp,
        trendlineLinear: {
          lineStyle: "line",
          width: 1
        }
      }]
    },
    options: {
      plugins: {
        title: {
          display: true,
          text: 'CO2 by Temperature levels'
        }
      },
      scales: {
        y: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'CO2, ppm'
          }
        },
        x: {
          type: 'linear',
          display: true,
          position: 'left',
          title: {
            display: true,
            text: 'Temperature, C'
          }
        }
      }
    }
  });
};
