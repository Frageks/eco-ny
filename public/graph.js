const loadData = async () => {
  const response = await fetch("http://www.eco-ny.uno/get-statistics");
  return await response.json();
}

document.addEventListener('DOMContentLoaded', async function() {
  let data = await loadData();
  data = data.filter(row => row.datetime < 1705352833);

  countOverallValues(data);
  countHourlyValues(data);
  countWindDirValues(data);
  countHumidityValues(data);
  countTemperatureValues(data);
  countWeatherValues(data);
}, false);
